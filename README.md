## Sign-up and authentication portal

This is the codebase for a web application hosted [here](https://beyonicauth.herokuapp.com). It allows registration of users to a platform and demonstrates the use of two factor authentication.

This is a Python Django app running on `Django 2`. All Python package dependencies can be gleaned from the `Pipfile`.

### Deliverables
1. Link to the live project: https://beyonicauth.herokuapp.com
2. Repository: https://gitlab.com/erickmbwana/beyoauth

### Local Installation
The project requires Python 3.5  or newer. Package and virtual environment management uses the excellent [pipenv](https://docs.pipenv.org/) package.

Clone this repo:
`git clone https://gitlab.com/erickmbwana/beyoauth`

Within the project folder span up a new virtual environment using Python three:

`pipenv shell --three`

Install all requirements as specified in the Pipfile:

`pipenv install`

The project uses an environment variable in determining whether to  use production or  development settings,
defaulting to production settings if the environment variable is not found.
To use the development settings add the following environment variable:

`DJANGO_MODE=DEVELOPMENT`

After adding the environment variable, run migrations to persist changes in the database:

`python manage.py migrate`

By default the development configuration uses the Sqlite database for local development. The production site uses PostgreSQL.
To change from Sqlite to PostgreSQL in your local machine add a `local.py` file in the `settings/` folder and add the database config as it suits you local set up:

```python
# beyonic/settings/local.py
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'beyonic',
        'USER': 'XXXXX',
        'PORT': '5432',
        'HOST': 'localhost',
        'PASSWORD': 'XXXX'
    }
}

```

You can then run the project: `python manage.py runserver`

NOTE: When developing locally, instead of sending live SMSes, the tokens are sent to the console.

### Asset management
The project uses `npm` for front end asset management.
The front end dependencies are listed in `package.json`

You can upgrade/downgrade any of the front end dependencies using `npm` and thereafter copy the updated/downgraded
files to the project's static file directories using this command:

`npm run assets`

The project uses `Bootstrap 4`  as a CSS framework for basic styling.
