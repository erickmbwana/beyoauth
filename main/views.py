from django.conf import settings
from django.contrib.auth import get_user_model, login
from django.shortcuts import get_object_or_404
from django.urls import reverse_lazy
from django.views.generic import CreateView, FormView, TemplateView

from django_otp.oath import totp
from two_factor.views.mixins import OTPRequiredMixin

from .forms import TokenForm, UserCreationForm
from .models import Profile
from .utils import send_sms


class IndexView(OTPRequiredMixin, TemplateView):

    template_name = 'main/index.html'


class UserRegistrationView(CreateView):

    model = get_user_model()
    success_url = reverse_lazy('activate')
    form_class = UserCreationForm
    template_name = 'main/user_registration_form.html'

    def form_valid(self, form):
        phone_number = form.cleaned_data['phone_number']

        # generate_token
        no_digits = 6
        key = bytearray(settings.SECRET_KEY, 'utf-8')
        token = str(totp(key, digits=no_digits)).zfill(no_digits)

        # create inactive user and profile
        form.instance.is_active = False
        user = form.save()
        Profile.objects.create(phone_number=phone_number, user=user, token=token)

        # send activation token
        send_sms(phone_number, token)
        return super().form_valid(form)


class UserActivationView(FormView):

    form_class = TokenForm
    template_name = 'main/activate.html'
    success_url = reverse_lazy('index')

    def form_valid(self, form):
        username = form.cleaned_data['username']
        profile = get_object_or_404(Profile, user__username=username)
        profile.user.is_active = True
        profile.user.save()
        login(self.request, profile.user)
        return super().form_valid(form)
