import os

from django.conf import settings

import africastalking


def send_sms(phone_number, token):
    message = 'Your Beyonic Auth token is {}'.format(token)
    if os.environ.get('DJANGO_MODE', 'DEVELOPMENT') == 'PRODUCTION':
        africastalking.initialize(
            settings.AFRICASTALKING_USERNAME,
            settings.AFRICASTALKING_LIVE_KEY
        )
        sms = africastalking.SMS
        sms.send(message=message, recipients=[phone_number.as_e164])
    else:
        # In local environment
        print(message)
