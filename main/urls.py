from django.contrib.auth import views as auth_views
from django.urls import path

from . import views


urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('register/', views.UserRegistrationView.as_view(), name='register'),
    path('activate/', views.UserActivationView.as_view(), name='activate'),
    path('logout/', auth_views.logout_then_login, {'login_url': 'two_factor:login'}, name='logout'),
]
