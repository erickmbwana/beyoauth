import logging

from ..utils import send_sms


logger = logging.getLogger(__name__)


class AfricasTalking(object):

    @staticmethod
    def send_sms(device, token):
        send_sms(device.number, token)
