from django import forms

from django.contrib.auth.forms import UserCreationForm as BaseUserCreationForm
from phonenumber_field.formfields import PhoneNumberField

from .models import Profile


class UserCreationForm(BaseUserCreationForm):

    phone_number = PhoneNumberField()

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['password1'].help_text = ''
        self.fields['phone_number'].help_text = 'A text message will be sent to this phone number'


class TokenForm(forms.Form):

    username = forms.CharField()
    token = forms.CharField()

    def clean(self):
        super().clean()
        token = self.cleaned_data['token']
        username = self.cleaned_data['username']
        if not Profile.objects.filter(user__username=username, token=token).exists():
            raise forms.ValidationError('Sorry, no account can be found. Please check your username and  token and try again')
