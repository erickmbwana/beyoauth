import os

import dj_database_url
from .base import DATABASES


db_from_env = dj_database_url.config()
DATABASES['default'].update(db_from_env)

DEBUG = False
ALLOWED_HOSTS = ['beyonicauth.herokuapp.com', ]
SECRET_KEY = os.environ.get('SECRET_KEY')
STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

TWO_FACTOR_SMS_GATEWAY = 'main.gateways.africastalking.AfricasTalking'

AFRICASTALKING_USERNAME = os.environ.get('AFRICASTALKING_USERNAME')
AFRICASTALKING_SANDBOX_KEY = os.environ.get('AFRICASTALKING_SANDBOX_KEY')
AFRICASTALKING_LIVE_KEY = os.environ.get('AFRICASTALKING_LIVE_KEY')
